/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaceproject;

/**
 *
 * @author a
 */
public class Human extends LandAnimal {

    private String name;

    public Human(String name) {
        super("Human", 2);
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println("Human name is : " + name + " Can run ");
    }

    @Override
    public void eat() {
        System.out.println("Human name is : " + name + " Can eat ");
    }

    @Override
    public void speak() {
        System.out.println("Human name is : " + name + " Can speak ");
    }

    @Override
    public void sleep() {
        System.out.println("Human name is : " + name + " Can sleep ");
    }

}
